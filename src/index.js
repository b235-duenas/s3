const express = require("express");

const app = express();
const port = 4000;

app.use(express.json());

let users = [
  {
    name: "Jojo Joestar",
    age: 25,
    username: "Jojo",
  },
  {
    name: "Dio Brando",
    age: 23,
    username: "Dio",
  },
  {
    name: "Jotaro Kujo",
    age: 28,
    username: "Jotaro",
  },
];

let artists = [
  {
    name: "Parokya ni Edgar",
    songs: ["Sampip", "Silvertoes"],
    album: "Buruguduystunstugudunstuy",
    isActive: true,
  },
  {
    name: "Eraserheads",
    songs: ["Shake Yer Head", "Ligaya"],
    album: "Ultraelectromagneticpop!",
    isActive: true,
  },
  {
    name: "Siakol",
    songs: ["Peksman", "Bakit Ba?"],
    album: "Tayo Na Sa Paraiso",
    isActive: true,
  },
];

app.get("/users", (req, res) => {
  console.log(users);
  return res.send(users);
});

app.post("/users", (req, res) => {
  // add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)
  // hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
  if (!req.body.hasOwnProperty("name") || !req.body.hasOwnProperty("age")) {
    return res
      .status(400)
      .send({ error: "Bad Request - missing required parameter NAME or AGE" });
  }
});

//Activity
app.get("/artists", (req, res) => {
  console.log(artists);
  return res.send(artists);
});

app.post("/artists", (req, res) => {
  if (
    !req.body.hasOwnProperty("name") ||
    !req.body.hasOwnProperty("songs") ||
    !req.body.hasOwnProperty("album") ||
    req.body.isActive === false
  ) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter NAME, ALBUM or SONGS",
    });
  }
});

app.listen(port, () => console.log(`Server running at port: ${port}`));
